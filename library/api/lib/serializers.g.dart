// GENERATED CODE - DO NOT MODIFY BY HAND

part of serializers;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializers _$serializers = (new Serializers().toBuilder()
      ..add(AuthUserDto.serializer)
      ..add(EditProductDto.serializer)
      ..add(InlineObject.serializer)
      ..add(InlineObject1.serializer)
      ..add(MyFile.serializer)
      ..add(PictureDto.serializer)
      ..add(ProductDto.serializer)
      ..add(ProductListDto.serializer)
      ..add(ProductSavedDto.serializer)
      ..add(SellerDto.serializer)
      ..add(UserCredentialsDto.serializer)
      ..add(UserDto.serializer)
      ..add(UserRegisterDto.serializer)
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(PictureDto)]),
          () => new ListBuilder<PictureDto>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(PictureDto)]),
          () => new ListBuilder<PictureDto>()))
    .build();

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
