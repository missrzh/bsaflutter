import 'dart:async';
import 'dart:io';
import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/serializer.dart';

import 'package:customserverapi/model/my_file.dart';
import 'package:customserverapi/model/picture_dto.dart';
import 'dart:typed_data';
import 'package:customserverapi/api_util.dart';

class PicturesApi {
  final Dio _dio;
  Serializers _serializers;

  PicturesApi(this._dio, this._serializers);

  ///
  ///
  ///
  Future<Response<PictureDto>> apiProductsProductIdPicturesAsStringPost(
    int productId, {
    MyFile myFile,
    CancelToken cancelToken,
    Map<String, String> headers,
  }) async {
    String _path = "/api/Products/{productId}/Pictures/asString"
        .replaceAll("{" r'productId' "}", productId.toString());

    Map<String, dynamic> queryParams = {};
    Map<String, String> headerParams = Map.from(headers ?? {});
    dynamic bodyData;

    queryParams.removeWhere((key, value) => value == null);
    headerParams.removeWhere((key, value) => value == null);

    List<String> contentTypes = [
      "application/json",
      "text/json",
      "application/_*+json"
    ];

    var serializedBody = _serializers.serialize(myFile);
    var jsonmyFile = json.encode(serializedBody);
    bodyData = jsonmyFile;

    return _dio
        .request(
      _path,
      queryParameters: queryParams,
      data: bodyData,
      options: Options(
        method: 'post'.toUpperCase(),
        headers: headerParams,
        contentType:
            contentTypes.isNotEmpty ? contentTypes[0] : "application/json",
      ),
      cancelToken: cancelToken,
    )
        .then((response) {
      var serializer = _serializers.serializerForType(PictureDto);
      var data =
          _serializers.deserializeWith<PictureDto>(serializer, response.data);

      return Response<PictureDto>(
        data: data,
        headers: response.headers,
        requestOptions: response.requestOptions,
        redirects: response.redirects,
        statusCode: response.statusCode,
        statusMessage: response.statusMessage,
        extra: response.extra,
      );
    });
  }

  ///
  ///
  ///
  Future<Response> apiProductsProductIdPicturesIdDelete(
    int id,
    int productId, {
    CancelToken cancelToken,
    Map<String, String> headers,
  }) async {
    String _path = "/api/Products/{productId}/Pictures/{id}"
        .replaceAll("{" r'id' "}", id.toString())
        .replaceAll("{" r'productId' "}", productId.toString());

    Map<String, dynamic> queryParams = {};
    Map<String, String> headerParams = Map.from(headers ?? {});
    dynamic bodyData;

    queryParams.removeWhere((key, value) => value == null);
    headerParams.removeWhere((key, value) => value == null);

    List<String> contentTypes = [];

    return _dio.request(
      _path,
      queryParameters: queryParams,
      data: bodyData,
      options: Options(
        method: 'delete'.toUpperCase(),
        headers: headerParams,
        contentType:
            contentTypes.isNotEmpty ? contentTypes[0] : "application/json",
      ),
      cancelToken: cancelToken,
    );
  }

  ///
  ///
  ///
  Future<Response<PictureDto>> apiProductsProductIdPicturesPost(
    int productId, {
    Uint8List file,
    CancelToken cancelToken,
    Map<String, String> headers,
  }) async {
    String _path = "/api/Products/{productId}/Pictures"
        .replaceAll("{" r'productId' "}", productId.toString());

    Map<String, dynamic> queryParams = {};
    Map<String, String> headerParams = Map.from(headers ?? {});
    dynamic bodyData;

    queryParams.removeWhere((key, value) => value == null);
    headerParams.removeWhere((key, value) => value == null);

    List<String> contentTypes = ["multipart/form-data"];

    Map<String, dynamic> formData = {};
    if (file != null) {
      formData[r'file'] = MultipartFile.fromBytes(file, filename: r'file');
    }
    bodyData = FormData.fromMap(formData);

    return _dio
        .request(
      _path,
      queryParameters: queryParams,
      data: bodyData,
      options: Options(
        method: 'post'.toUpperCase(),
        headers: headerParams,
        contentType:
            contentTypes.isNotEmpty ? contentTypes[0] : "application/json",
      ),
      cancelToken: cancelToken,
    )
        .then((response) {
      var serializer = _serializers.serializerForType(PictureDto);
      var data =
          _serializers.deserializeWith<PictureDto>(serializer, response.data);

      return Response<PictureDto>(
        data: data,
        headers: response.headers,
        requestOptions: response.requestOptions,
        redirects: response.redirects,
        statusCode: response.statusCode,
        statusMessage: response.statusMessage,
        extra: response.extra,
      );
    });
  }
}
