import 'dart:async';
import 'dart:io';
import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/serializer.dart';

import 'package:customserverapi/model/user_credentials_dto.dart';
import 'package:customserverapi/model/auth_user_dto.dart';
import 'package:customserverapi/model/user_dto.dart';
import 'package:customserverapi/model/user_register_dto.dart';

class AuthApi {
  final Dio _dio;
  Serializers _serializers;

  AuthApi(this._dio, this._serializers);

  ///
  ///
  ///
  Future<Response<AuthUserDto>> apiAuthLoginPost({
    UserCredentialsDto userCredentialsDto,
    CancelToken cancelToken,
    Map<String, String> headers,
  }) async {
    String _path = "/api/Auth/login";

    Map<String, dynamic> queryParams = {};
    Map<String, String> headerParams = Map.from(headers ?? {});
    dynamic bodyData;

    queryParams.removeWhere((key, value) => value == null);
    headerParams.removeWhere((key, value) => value == null);

    List<String> contentTypes = [
      "application/json",
      "text/json",
      "application/_*+json"
    ];

    var serializedBody = _serializers.serialize(userCredentialsDto);
    var jsonuserCredentialsDto = json.encode(serializedBody);
    bodyData = jsonuserCredentialsDto;

    return _dio
        .request(
      _path,
      queryParameters: queryParams,
      data: bodyData,
      options: Options(
        method: 'post'.toUpperCase(),
        headers: headerParams,
        contentType:
            contentTypes.isNotEmpty ? contentTypes[0] : "application/json",
      ),
      cancelToken: cancelToken,
    )
        .then((response) {
      var serializer = _serializers.serializerForType(AuthUserDto);
      var data =
          _serializers.deserializeWith<AuthUserDto>(serializer, response.data);

      return Response<AuthUserDto>(
        data: data,
        headers: response.headers,
        requestOptions: response.requestOptions,
        redirects: response.redirects,
        statusCode: response.statusCode,
        statusMessage: response.statusMessage,
        extra: response.extra,
      );
    });
  }

  ///
  ///
  ///
  Future<Response<UserDto>> apiAuthRegisterPost({
    UserRegisterDto userRegisterDto,
    CancelToken cancelToken,
    Map<String, String> headers,
  }) async {
    String _path = "/api/Auth/register";

    Map<String, dynamic> queryParams = {};
    Map<String, String> headerParams = Map.from(headers ?? {});
    dynamic bodyData;

    queryParams.removeWhere((key, value) => value == null);
    headerParams.removeWhere((key, value) => value == null);

    List<String> contentTypes = [
      "application/json",
      "text/json",
      "application/_*+json"
    ];

    var serializedBody = _serializers.serialize(userRegisterDto);
    var jsonuserRegisterDto = json.encode(serializedBody);
    bodyData = jsonuserRegisterDto;

    return _dio
        .request(
      _path,
      queryParameters: queryParams,
      data: bodyData,
      options: Options(
        method: 'post'.toUpperCase(),
        headers: headerParams,
        contentType:
            contentTypes.isNotEmpty ? contentTypes[0] : "application/json",
      ),
      cancelToken: cancelToken,
    )
        .then((response) {
      var serializer = _serializers.serializerForType(UserDto);
      var data =
          _serializers.deserializeWith<UserDto>(serializer, response.data);

      return Response<UserDto>(
        data: data,
        headers: response.headers,
        requestOptions: response.requestOptions,
        redirects: response.redirects,
        statusCode: response.statusCode,
        statusMessage: response.statusMessage,
        extra: response.extra,
      );
    });
  }
}
