import 'dart:async';
import 'dart:io';
import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/serializer.dart';

import 'package:customserverapi/model/product_dto.dart';
import 'package:customserverapi/model/product_list_dto.dart';
import 'package:customserverapi/model/edit_product_dto.dart';
import 'package:customserverapi/model/product_saved_dto.dart';

class ProductsApi {
  final Dio _dio;
  Serializers _serializers;

  ProductsApi(this._dio, this._serializers);

  ///
  ///
  ///
  Future<Response<List<ProductListDto>>> apiProductsGet({
    int page,
    int perPage,
    String filter,
    CancelToken cancelToken,
    Map<String, String> headers,
  }) async {
    String _path = "/api/Products";

    Map<String, dynamic> queryParams = {};
    Map<String, String> headerParams = Map.from(headers ?? {});
    dynamic bodyData;

    queryParams[r'page'] = page;
    queryParams[r'perPage'] = perPage;
    queryParams[r'filter'] = filter;
    queryParams.removeWhere((key, value) => value == null);
    headerParams.removeWhere((key, value) => value == null);

    List<String> contentTypes = [];

    return _dio
        .request(
      _path,
      queryParameters: queryParams,
      data: bodyData,
      options: Options(
        method: 'get'.toUpperCase(),
        headers: headerParams,
        contentType:
            contentTypes.isNotEmpty ? contentTypes[0] : "application/json",
      ),
      cancelToken: cancelToken,
    )
        .then((response) {
      final FullType type =
          const FullType(BuiltList, const [const FullType(ProductListDto)]);
      BuiltList<ProductListDto> dataList =
          _serializers.deserialize(response.data, specifiedType: type);
      var data = dataList.toList();

      return Response<List<ProductListDto>>(
        data: data,
        headers: response.headers,
        requestOptions: response.requestOptions,
        redirects: response.redirects,
        statusCode: response.statusCode,
        statusMessage: response.statusMessage,
        extra: response.extra,
      );
    });
  }

  ///
  ///
  ///
  Future<Response> apiProductsIdDelete(
    int id, {
    CancelToken cancelToken,
    Map<String, String> headers,
  }) async {
    String _path =
        "/api/Products/{id}".replaceAll("{" r'id' "}", id.toString());

    Map<String, dynamic> queryParams = {};
    Map<String, String> headerParams = Map.from(headers ?? {});
    dynamic bodyData;

    queryParams.removeWhere((key, value) => value == null);
    headerParams.removeWhere((key, value) => value == null);

    List<String> contentTypes = [];

    return _dio.request(
      _path,
      queryParameters: queryParams,
      data: bodyData,
      options: Options(
        method: 'delete'.toUpperCase(),
        headers: headerParams,
        contentType:
            contentTypes.isNotEmpty ? contentTypes[0] : "application/json",
      ),
      cancelToken: cancelToken,
    );
  }

  ///
  ///
  ///
  Future<Response<ProductDto>> apiProductsIdGet(
    int id, {
    CancelToken cancelToken,
    Map<String, String> headers,
  }) async {
    String _path =
        "/api/Products/{id}".replaceAll("{" r'id' "}", id.toString());

    Map<String, dynamic> queryParams = {};
    Map<String, String> headerParams = Map.from(headers ?? {});
    dynamic bodyData;

    queryParams.removeWhere((key, value) => value == null);
    headerParams.removeWhere((key, value) => value == null);

    List<String> contentTypes = [];

    return _dio
        .request(
      _path,
      queryParameters: queryParams,
      data: bodyData,
      options: Options(
        method: 'get'.toUpperCase(),
        headers: headerParams,
        contentType:
            contentTypes.isNotEmpty ? contentTypes[0] : "application/json",
      ),
      cancelToken: cancelToken,
    )
        .then((response) {
      var serializer = _serializers.serializerForType(ProductDto);
      var data =
          _serializers.deserializeWith<ProductDto>(serializer, response.data);

      return Response<ProductDto>(
        data: data,
        headers: response.headers,
        requestOptions: response.requestOptions,
        redirects: response.redirects,
        statusCode: response.statusCode,
        statusMessage: response.statusMessage,
        extra: response.extra,
      );
    });
  }

  ///
  ///
  ///
  Future<Response<ProductSavedDto>> apiProductsIdPut(
    int id, {
    EditProductDto editProductDto,
    CancelToken cancelToken,
    Map<String, String> headers,
  }) async {
    String _path =
        "/api/Products/{id}".replaceAll("{" r'id' "}", id.toString());

    Map<String, dynamic> queryParams = {};
    Map<String, String> headerParams = Map.from(headers ?? {});
    dynamic bodyData;

    queryParams.removeWhere((key, value) => value == null);
    headerParams.removeWhere((key, value) => value == null);

    List<String> contentTypes = [
      "application/json",
      "text/json",
      "application/_*+json"
    ];

    var serializedBody = _serializers.serialize(editProductDto);
    var jsoneditProductDto = json.encode(serializedBody);
    bodyData = jsoneditProductDto;

    return _dio
        .request(
      _path,
      queryParameters: queryParams,
      data: bodyData,
      options: Options(
        method: 'put'.toUpperCase(),
        headers: headerParams,
        contentType:
            contentTypes.isNotEmpty ? contentTypes[0] : "application/json",
      ),
      cancelToken: cancelToken,
    )
        .then((response) {
      var serializer = _serializers.serializerForType(ProductSavedDto);
      var data = _serializers.deserializeWith<ProductSavedDto>(
          serializer, response.data);

      return Response<ProductSavedDto>(
        data: data,
        headers: response.headers,
        requestOptions: response.requestOptions,
        redirects: response.redirects,
        statusCode: response.statusCode,
        statusMessage: response.statusMessage,
        extra: response.extra,
      );
    });
  }

  ///
  ///
  ///
  Future<Response<ProductSavedDto>> apiProductsPost({
    EditProductDto editProductDto,
    CancelToken cancelToken,
    Map<String, String> headers,
  }) async {
    String _path = "/api/Products";

    Map<String, dynamic> queryParams = {};
    Map<String, String> headerParams = Map.from(headers ?? {});
    dynamic bodyData;

    queryParams.removeWhere((key, value) => value == null);
    headerParams.removeWhere((key, value) => value == null);

    List<String> contentTypes = [
      "application/json",
      "text/json",
      "application/_*+json"
    ];

    var serializedBody = _serializers.serialize(editProductDto);
    var jsoneditProductDto = json.encode(serializedBody);
    bodyData = jsoneditProductDto;

    return _dio
        .request(
      _path,
      queryParameters: queryParams,
      data: bodyData,
      options: Options(
        method: 'post'.toUpperCase(),
        headers: headerParams,
        contentType:
            contentTypes.isNotEmpty ? contentTypes[0] : "application/json",
      ),
      cancelToken: cancelToken,
    )
        .then((response) {
      var serializer = _serializers.serializerForType(ProductSavedDto);
      var data = _serializers.deserializeWith<ProductSavedDto>(
          serializer, response.data);

      return Response<ProductSavedDto>(
        data: data,
        headers: response.headers,
        requestOptions: response.requestOptions,
        redirects: response.redirects,
        statusCode: response.statusCode,
        statusMessage: response.statusMessage,
        extra: response.extra,
      );
    });
  }
}
