import 'dart:async';
import 'dart:io';
import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/serializer.dart';

import 'package:customserverapi/model/my_file.dart';
import 'package:customserverapi/model/user_dto.dart';
import 'dart:typed_data';
import 'package:customserverapi/api_util.dart';

class UsersApi {
  final Dio _dio;
  Serializers _serializers;

  UsersApi(this._dio, this._serializers);

  ///
  ///
  ///
  Future<Response<String>> apiUsersAvatarAsStringPost({
    MyFile myFile,
    CancelToken cancelToken,
    Map<String, String> headers,
  }) async {
    String _path = "/api/Users/avatar/asString";

    Map<String, dynamic> queryParams = {};
    Map<String, String> headerParams = Map.from(headers ?? {});
    dynamic bodyData;

    queryParams.removeWhere((key, value) => value == null);
    headerParams.removeWhere((key, value) => value == null);

    List<String> contentTypes = [
      "application/json",
      "text/json",
      "application/_*+json"
    ];

    var serializedBody = _serializers.serialize(myFile);
    var jsonmyFile = json.encode(serializedBody);
    bodyData = jsonmyFile;

    return _dio
        .request(
      _path,
      queryParameters: queryParams,
      data: bodyData,
      options: Options(
        method: 'post'.toUpperCase(),
        headers: headerParams,
        contentType:
            contentTypes.isNotEmpty ? contentTypes[0] : "application/json",
      ),
      cancelToken: cancelToken,
    )
        .then((response) {
      var serializer = _serializers.serializerForType(String);
      var data =
          _serializers.deserializeWith<String>(serializer, response.data);

      return Response<String>(
        data: data,
        headers: response.headers,
        requestOptions: response.requestOptions,
        redirects: response.redirects,
        statusCode: response.statusCode,
        statusMessage: response.statusMessage,
        extra: response.extra,
      );
    });
  }

  ///
  ///
  ///
  Future<Response<String>> apiUsersAvatarPost({
    Uint8List file,
    CancelToken cancelToken,
    Map<String, String> headers,
  }) async {
    String _path = "/api/Users/avatar";

    Map<String, dynamic> queryParams = {};
    Map<String, String> headerParams = Map.from(headers ?? {});
    dynamic bodyData;

    queryParams.removeWhere((key, value) => value == null);
    headerParams.removeWhere((key, value) => value == null);

    List<String> contentTypes = ["multipart/form-data"];

    Map<String, dynamic> formData = {};
    if (file != null) {
      formData[r'file'] = MultipartFile.fromBytes(file, filename: r'file');
    }
    bodyData = FormData.fromMap(formData);

    return _dio
        .request(
      _path,
      queryParameters: queryParams,
      data: bodyData,
      options: Options(
        method: 'post'.toUpperCase(),
        headers: headerParams,
        contentType:
            contentTypes.isNotEmpty ? contentTypes[0] : "application/json",
      ),
      cancelToken: cancelToken,
    )
        .then((response) {
      var serializer = _serializers.serializerForType(String);
      var data =
          _serializers.deserializeWith<String>(serializer, response.data);

      return Response<String>(
        data: data,
        headers: response.headers,
        requestOptions: response.requestOptions,
        redirects: response.redirects,
        statusCode: response.statusCode,
        statusMessage: response.statusMessage,
        extra: response.extra,
      );
    });
  }

  ///
  ///
  ///
  Future<Response<UserDto>> apiUsersDetailsGet({
    CancelToken cancelToken,
    Map<String, String> headers,
  }) async {
    String _path = "/api/Users/details";

    Map<String, dynamic> queryParams = {};
    Map<String, String> headerParams = Map.from(headers ?? {});
    dynamic bodyData;

    queryParams.removeWhere((key, value) => value == null);
    headerParams.removeWhere((key, value) => value == null);

    List<String> contentTypes = [];

    return _dio
        .request(
      _path,
      queryParameters: queryParams,
      data: bodyData,
      options: Options(
        method: 'get'.toUpperCase(),
        headers: headerParams,
        contentType:
            contentTypes.isNotEmpty ? contentTypes[0] : "application/json",
      ),
      cancelToken: cancelToken,
    )
        .then((response) {
      var serializer = _serializers.serializerForType(UserDto);
      var data =
          _serializers.deserializeWith<UserDto>(serializer, response.data);

      return Response<UserDto>(
        data: data,
        headers: response.headers,
        requestOptions: response.requestOptions,
        redirects: response.redirects,
        statusCode: response.statusCode,
        statusMessage: response.statusMessage,
        extra: response.extra,
      );
    });
  }
}
