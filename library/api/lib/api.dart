library openapi.api;

import 'package:dio/dio.dart';
import 'package:built_value/serializer.dart';
import 'package:customserverapi/serializers.dart';
import 'package:customserverapi/api/auth_api.dart';
import 'package:customserverapi/api/pictures_api.dart';
import 'package:customserverapi/api/products_api.dart';
import 'package:customserverapi/api/users_api.dart';

class Openapi {
  Dio dio;
  Serializers serializers;
  String basePath = "https://bsa-marketplace.azurewebsites.net";

  Openapi({this.dio, Serializers serializers}) {
    if (dio == null) {
      BaseOptions options = new BaseOptions(
        baseUrl: basePath,
        connectTimeout: 5000,
        receiveTimeout: 3000,
      );
      this.dio = new Dio(options);
    }

    this.serializers = serializers ?? standardSerializers;
  }

  /**
    * Get AuthApi instance, base route and serializer can be overridden by a given but be careful,
    * by doing that all interceptors will not be executed
    */
  AuthApi getAuthApi() {
    return AuthApi(dio, serializers);
  }

  /**
    * Get PicturesApi instance, base route and serializer can be overridden by a given but be careful,
    * by doing that all interceptors will not be executed
    */
  PicturesApi getPicturesApi() {
    return PicturesApi(dio, serializers);
  }

  /**
    * Get ProductsApi instance, base route and serializer can be overridden by a given but be careful,
    * by doing that all interceptors will not be executed
    */
  ProductsApi getProductsApi() {
    return ProductsApi(dio, serializers);
  }

  /**
    * Get UsersApi instance, base route and serializer can be overridden by a given but be careful,
    * by doing that all interceptors will not be executed
    */
  UsersApi getUsersApi() {
    return UsersApi(dio, serializers);
  }
}
