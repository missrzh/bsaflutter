library serializers;

import 'package:built_value/serializer.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/json_object.dart';
import 'package:built_value/standard_json_plugin.dart';

import 'package:customserverapi/model/auth_user_dto.dart';
import 'package:customserverapi/model/edit_product_dto.dart';
import 'package:customserverapi/model/inline_object.dart';
import 'package:customserverapi/model/inline_object1.dart';
import 'package:customserverapi/model/my_file.dart';
import 'package:customserverapi/model/picture_dto.dart';
import 'package:customserverapi/model/product_dto.dart';
import 'package:customserverapi/model/product_list_dto.dart';
import 'package:customserverapi/model/product_saved_dto.dart';
import 'package:customserverapi/model/seller_dto.dart';
import 'package:customserverapi/model/user_credentials_dto.dart';
import 'package:customserverapi/model/user_dto.dart';
import 'package:customserverapi/model/user_register_dto.dart';

part 'serializers.g.dart';

@SerializersFor(const [
  AuthUserDto,
  EditProductDto,
  InlineObject,
  InlineObject1,
  MyFile,
  PictureDto,
  ProductDto,
  ProductListDto,
  ProductSavedDto,
  SellerDto,
  UserCredentialsDto,
  UserDto,
  UserRegisterDto,
])

//allow all models to be serialized within a list
Serializers serializers = (_$serializers.toBuilder()
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(AuthUserDto)]),
          () => new ListBuilder<AuthUserDto>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(EditProductDto)]),
          () => new ListBuilder<EditProductDto>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(InlineObject)]),
          () => new ListBuilder<InlineObject>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(InlineObject1)]),
          () => new ListBuilder<InlineObject1>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(MyFile)]),
          () => new ListBuilder<MyFile>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(PictureDto)]),
          () => new ListBuilder<PictureDto>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(ProductDto)]),
          () => new ListBuilder<ProductDto>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(ProductListDto)]),
          () => new ListBuilder<ProductListDto>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(ProductSavedDto)]),
          () => new ListBuilder<ProductSavedDto>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(SellerDto)]),
          () => new ListBuilder<SellerDto>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(UserCredentialsDto)]),
          () => new ListBuilder<UserCredentialsDto>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(UserDto)]),
          () => new ListBuilder<UserDto>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(UserRegisterDto)]),
          () => new ListBuilder<UserRegisterDto>()))
    .build();

Serializers standardSerializers =
    (serializers.toBuilder()..addPlugin(StandardJsonPlugin())).build();
