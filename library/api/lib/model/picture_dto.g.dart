// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'picture_dto.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<PictureDto> _$pictureDtoSerializer = new _$PictureDtoSerializer();

class _$PictureDtoSerializer implements StructuredSerializer<PictureDto> {
  @override
  final Iterable<Type> types = const [PictureDto, _$PictureDto];
  @override
  final String wireName = 'PictureDto';

  @override
  Iterable<Object> serialize(Serializers serializers, PictureDto object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.id != null) {
      result
        ..add('id')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(int)));
    }
    if (object.url != null) {
      result
        ..add('url')
        ..add(serializers.serialize(object.url,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  PictureDto deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new PictureDtoBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'url':
          result.url = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$PictureDto extends PictureDto {
  @override
  final int id;
  @override
  final String url;

  factory _$PictureDto([void Function(PictureDtoBuilder) updates]) =>
      (new PictureDtoBuilder()..update(updates)).build();

  _$PictureDto._({this.id, this.url}) : super._();

  @override
  PictureDto rebuild(void Function(PictureDtoBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  PictureDtoBuilder toBuilder() => new PictureDtoBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is PictureDto && id == other.id && url == other.url;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, id.hashCode), url.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('PictureDto')
          ..add('id', id)
          ..add('url', url))
        .toString();
  }
}

class PictureDtoBuilder implements Builder<PictureDto, PictureDtoBuilder> {
  _$PictureDto _$v;

  int _id;
  int get id => _$this._id;
  set id(int id) => _$this._id = id;

  String _url;
  String get url => _$this._url;
  set url(String url) => _$this._url = url;

  PictureDtoBuilder();

  PictureDtoBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _url = _$v.url;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(PictureDto other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$PictureDto;
  }

  @override
  void update(void Function(PictureDtoBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$PictureDto build() {
    final _$result = _$v ?? new _$PictureDto._(id: id, url: url);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
