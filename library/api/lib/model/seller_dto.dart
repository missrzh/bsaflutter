        import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'seller_dto.g.dart';

abstract class SellerDto implements Built<SellerDto, SellerDtoBuilder> {

    
        @nullable
    @BuiltValueField(wireName: r'name')
    String get name;
    
        @nullable
    @BuiltValueField(wireName: r'email')
    String get email;
    
        @nullable
    @BuiltValueField(wireName: r'phoneNumber')
    String get phoneNumber;
    
        @nullable
    @BuiltValueField(wireName: r'avatar')
    String get avatar;

    // Boilerplate code needed to wire-up generated code
    SellerDto._();

    factory SellerDto([updates(SellerDtoBuilder b)]) = _$SellerDto;
    static Serializer<SellerDto> get serializer => _$sellerDtoSerializer;

}

