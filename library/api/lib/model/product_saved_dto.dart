import 'package:built_collection/built_collection.dart';
import 'package:customserverapi/model/picture_dto.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'product_saved_dto.g.dart';

abstract class ProductSavedDto
    implements Built<ProductSavedDto, ProductSavedDtoBuilder> {
  @nullable
  @BuiltValueField(wireName: r'id')
  int get id;

  @nullable
  @BuiltValueField(wireName: r'title')
  String get title;

  @nullable
  @BuiltValueField(wireName: r'price')
  double get price;

  @nullable
  @BuiltValueField(wireName: r'description')
  String get description;

  @nullable
  @BuiltValueField(wireName: r'createdAt')
  DateTime get createdAt;

  @nullable
  @BuiltValueField(wireName: r'pictures')
  BuiltList<PictureDto> get pictures;

  // Boilerplate code needed to wire-up generated code
  ProductSavedDto._();

  factory ProductSavedDto([updates(ProductSavedDtoBuilder b)]) =
      _$ProductSavedDto;
  static Serializer<ProductSavedDto> get serializer =>
      _$productSavedDtoSerializer;
}
