        import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'user_credentials_dto.g.dart';

abstract class UserCredentialsDto implements Built<UserCredentialsDto, UserCredentialsDtoBuilder> {

    
        @nullable
    @BuiltValueField(wireName: r'email')
    String get email;
    
        @nullable
    @BuiltValueField(wireName: r'password')
    String get password;

    // Boilerplate code needed to wire-up generated code
    UserCredentialsDto._();

    factory UserCredentialsDto([updates(UserCredentialsDtoBuilder b)]) = _$UserCredentialsDto;
    static Serializer<UserCredentialsDto> get serializer => _$userCredentialsDtoSerializer;

}

