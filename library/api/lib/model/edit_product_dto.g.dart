// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'edit_product_dto.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<EditProductDto> _$editProductDtoSerializer =
    new _$EditProductDtoSerializer();

class _$EditProductDtoSerializer
    implements StructuredSerializer<EditProductDto> {
  @override
  final Iterable<Type> types = const [EditProductDto, _$EditProductDto];
  @override
  final String wireName = 'EditProductDto';

  @override
  Iterable<Object> serialize(Serializers serializers, EditProductDto object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.title != null) {
      result
        ..add('title')
        ..add(serializers.serialize(object.title,
            specifiedType: const FullType(String)));
    }
    if (object.price != null) {
      result
        ..add('price')
        ..add(serializers.serialize(object.price,
            specifiedType: const FullType(double)));
    }
    if (object.description != null) {
      result
        ..add('description')
        ..add(serializers.serialize(object.description,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  EditProductDto deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new EditProductDtoBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'title':
          result.title = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'price':
          result.price = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
        case 'description':
          result.description = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$EditProductDto extends EditProductDto {
  @override
  final String title;
  @override
  final double price;
  @override
  final String description;

  factory _$EditProductDto([void Function(EditProductDtoBuilder) updates]) =>
      (new EditProductDtoBuilder()..update(updates)).build();

  _$EditProductDto._({this.title, this.price, this.description}) : super._();

  @override
  EditProductDto rebuild(void Function(EditProductDtoBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  EditProductDtoBuilder toBuilder() =>
      new EditProductDtoBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is EditProductDto &&
        title == other.title &&
        price == other.price &&
        description == other.description;
  }

  @override
  int get hashCode {
    return $jf(
        $jc($jc($jc(0, title.hashCode), price.hashCode), description.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('EditProductDto')
          ..add('title', title)
          ..add('price', price)
          ..add('description', description))
        .toString();
  }
}

class EditProductDtoBuilder
    implements Builder<EditProductDto, EditProductDtoBuilder> {
  _$EditProductDto _$v;

  String _title;
  String get title => _$this._title;
  set title(String title) => _$this._title = title;

  double _price;
  double get price => _$this._price;
  set price(double price) => _$this._price = price;

  String _description;
  String get description => _$this._description;
  set description(String description) => _$this._description = description;

  EditProductDtoBuilder();

  EditProductDtoBuilder get _$this {
    if (_$v != null) {
      _title = _$v.title;
      _price = _$v.price;
      _description = _$v.description;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(EditProductDto other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$EditProductDto;
  }

  @override
  void update(void Function(EditProductDtoBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$EditProductDto build() {
    final _$result = _$v ??
        new _$EditProductDto._(
            title: title, price: price, description: description);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
