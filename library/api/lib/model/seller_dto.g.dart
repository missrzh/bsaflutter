// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'seller_dto.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<SellerDto> _$sellerDtoSerializer = new _$SellerDtoSerializer();

class _$SellerDtoSerializer implements StructuredSerializer<SellerDto> {
  @override
  final Iterable<Type> types = const [SellerDto, _$SellerDto];
  @override
  final String wireName = 'SellerDto';

  @override
  Iterable<Object> serialize(Serializers serializers, SellerDto object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.name != null) {
      result
        ..add('name')
        ..add(serializers.serialize(object.name,
            specifiedType: const FullType(String)));
    }
    if (object.email != null) {
      result
        ..add('email')
        ..add(serializers.serialize(object.email,
            specifiedType: const FullType(String)));
    }
    if (object.phoneNumber != null) {
      result
        ..add('phoneNumber')
        ..add(serializers.serialize(object.phoneNumber,
            specifiedType: const FullType(String)));
    }
    if (object.avatar != null) {
      result
        ..add('avatar')
        ..add(serializers.serialize(object.avatar,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  SellerDto deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new SellerDtoBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'email':
          result.email = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'phoneNumber':
          result.phoneNumber = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'avatar':
          result.avatar = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$SellerDto extends SellerDto {
  @override
  final String name;
  @override
  final String email;
  @override
  final String phoneNumber;
  @override
  final String avatar;

  factory _$SellerDto([void Function(SellerDtoBuilder) updates]) =>
      (new SellerDtoBuilder()..update(updates)).build();

  _$SellerDto._({this.name, this.email, this.phoneNumber, this.avatar})
      : super._();

  @override
  SellerDto rebuild(void Function(SellerDtoBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SellerDtoBuilder toBuilder() => new SellerDtoBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SellerDto &&
        name == other.name &&
        email == other.email &&
        phoneNumber == other.phoneNumber &&
        avatar == other.avatar;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, name.hashCode), email.hashCode), phoneNumber.hashCode),
        avatar.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('SellerDto')
          ..add('name', name)
          ..add('email', email)
          ..add('phoneNumber', phoneNumber)
          ..add('avatar', avatar))
        .toString();
  }
}

class SellerDtoBuilder implements Builder<SellerDto, SellerDtoBuilder> {
  _$SellerDto _$v;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  String _email;
  String get email => _$this._email;
  set email(String email) => _$this._email = email;

  String _phoneNumber;
  String get phoneNumber => _$this._phoneNumber;
  set phoneNumber(String phoneNumber) => _$this._phoneNumber = phoneNumber;

  String _avatar;
  String get avatar => _$this._avatar;
  set avatar(String avatar) => _$this._avatar = avatar;

  SellerDtoBuilder();

  SellerDtoBuilder get _$this {
    if (_$v != null) {
      _name = _$v.name;
      _email = _$v.email;
      _phoneNumber = _$v.phoneNumber;
      _avatar = _$v.avatar;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SellerDto other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$SellerDto;
  }

  @override
  void update(void Function(SellerDtoBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$SellerDto build() {
    final _$result = _$v ??
        new _$SellerDto._(
            name: name, email: email, phoneNumber: phoneNumber, avatar: avatar);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
