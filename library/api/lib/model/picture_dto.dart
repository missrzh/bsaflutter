        import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'picture_dto.g.dart';

abstract class PictureDto implements Built<PictureDto, PictureDtoBuilder> {

    
        @nullable
    @BuiltValueField(wireName: r'id')
    int get id;
    
        @nullable
    @BuiltValueField(wireName: r'url')
    String get url;

    // Boilerplate code needed to wire-up generated code
    PictureDto._();

    factory PictureDto([updates(PictureDtoBuilder b)]) = _$PictureDto;
    static Serializer<PictureDto> get serializer => _$pictureDtoSerializer;

}

