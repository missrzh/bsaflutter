// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'product_list_dto.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<ProductListDto> _$productListDtoSerializer =
    new _$ProductListDtoSerializer();

class _$ProductListDtoSerializer
    implements StructuredSerializer<ProductListDto> {
  @override
  final Iterable<Type> types = const [ProductListDto, _$ProductListDto];
  @override
  final String wireName = 'ProductListDto';

  @override
  Iterable<Object> serialize(Serializers serializers, ProductListDto object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.id != null) {
      result
        ..add('id')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(int)));
    }
    if (object.title != null) {
      result
        ..add('title')
        ..add(serializers.serialize(object.title,
            specifiedType: const FullType(String)));
    }
    if (object.price != null) {
      result
        ..add('price')
        ..add(serializers.serialize(object.price,
            specifiedType: const FullType(double)));
    }
    if (object.description != null) {
      result
        ..add('description')
        ..add(serializers.serialize(object.description,
            specifiedType: const FullType(String)));
    }
    if (object.picture != null) {
      result
        ..add('picture')
        ..add(serializers.serialize(object.picture,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  ProductListDto deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ProductListDtoBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'title':
          result.title = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'price':
          result.price = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
        case 'description':
          result.description = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'picture':
          result.picture = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$ProductListDto extends ProductListDto {
  @override
  final int id;
  @override
  final String title;
  @override
  final double price;
  @override
  final String description;
  @override
  final String picture;

  factory _$ProductListDto([void Function(ProductListDtoBuilder) updates]) =>
      (new ProductListDtoBuilder()..update(updates)).build();

  _$ProductListDto._(
      {this.id, this.title, this.price, this.description, this.picture})
      : super._();

  @override
  ProductListDto rebuild(void Function(ProductListDtoBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ProductListDtoBuilder toBuilder() =>
      new ProductListDtoBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ProductListDto &&
        id == other.id &&
        title == other.title &&
        price == other.price &&
        description == other.description &&
        picture == other.picture;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc($jc(0, id.hashCode), title.hashCode), price.hashCode),
            description.hashCode),
        picture.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ProductListDto')
          ..add('id', id)
          ..add('title', title)
          ..add('price', price)
          ..add('description', description)
          ..add('picture', picture))
        .toString();
  }
}

class ProductListDtoBuilder
    implements Builder<ProductListDto, ProductListDtoBuilder> {
  _$ProductListDto _$v;

  int _id;
  int get id => _$this._id;
  set id(int id) => _$this._id = id;

  String _title;
  String get title => _$this._title;
  set title(String title) => _$this._title = title;

  double _price;
  double get price => _$this._price;
  set price(double price) => _$this._price = price;

  String _description;
  String get description => _$this._description;
  set description(String description) => _$this._description = description;

  String _picture;
  String get picture => _$this._picture;
  set picture(String picture) => _$this._picture = picture;

  ProductListDtoBuilder();

  ProductListDtoBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _title = _$v.title;
      _price = _$v.price;
      _description = _$v.description;
      _picture = _$v.picture;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ProductListDto other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ProductListDto;
  }

  @override
  void update(void Function(ProductListDtoBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ProductListDto build() {
    final _$result = _$v ??
        new _$ProductListDto._(
            id: id,
            title: title,
            price: price,
            description: description,
            picture: picture);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
