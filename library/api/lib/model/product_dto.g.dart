// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'product_dto.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<ProductDto> _$productDtoSerializer = new _$ProductDtoSerializer();

class _$ProductDtoSerializer implements StructuredSerializer<ProductDto> {
  @override
  final Iterable<Type> types = const [ProductDto, _$ProductDto];
  @override
  final String wireName = 'ProductDto';

  @override
  Iterable<Object> serialize(Serializers serializers, ProductDto object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.id != null) {
      result
        ..add('id')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(int)));
    }
    if (object.title != null) {
      result
        ..add('title')
        ..add(serializers.serialize(object.title,
            specifiedType: const FullType(String)));
    }
    if (object.price != null) {
      result
        ..add('price')
        ..add(serializers.serialize(object.price,
            specifiedType: const FullType(double)));
    }
    if (object.description != null) {
      result
        ..add('description')
        ..add(serializers.serialize(object.description,
            specifiedType: const FullType(String)));
    }
    if (object.createdAt != null) {
      result
        ..add('createdAt')
        ..add(serializers.serialize(object.createdAt,
            specifiedType: const FullType(DateTime)));
    }
    if (object.pictures != null) {
      result
        ..add('pictures')
        ..add(serializers.serialize(object.pictures,
            specifiedType:
                const FullType(BuiltList, const [const FullType(PictureDto)])));
    }
    if (object.seller != null) {
      result
        ..add('seller')
        ..add(serializers.serialize(object.seller,
            specifiedType: const FullType(SellerDto)));
    }
    return result;
  }

  @override
  ProductDto deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ProductDtoBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'title':
          result.title = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'price':
          result.price = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
        case 'description':
          result.description = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'createdAt':
          result.createdAt = serializers.deserialize(value,
              specifiedType: const FullType(DateTime)) as DateTime;
          break;
        case 'pictures':
          result.pictures.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(PictureDto)]))
              as BuiltList<dynamic>);
          break;
        case 'seller':
          result.seller.replace(serializers.deserialize(value,
              specifiedType: const FullType(SellerDto)) as SellerDto);
          break;
      }
    }

    return result.build();
  }
}

class _$ProductDto extends ProductDto {
  @override
  final int id;
  @override
  final String title;
  @override
  final double price;
  @override
  final String description;
  @override
  final DateTime createdAt;
  @override
  final BuiltList<PictureDto> pictures;
  @override
  final SellerDto seller;

  factory _$ProductDto([void Function(ProductDtoBuilder) updates]) =>
      (new ProductDtoBuilder()..update(updates)).build();

  _$ProductDto._(
      {this.id,
      this.title,
      this.price,
      this.description,
      this.createdAt,
      this.pictures,
      this.seller})
      : super._();

  @override
  ProductDto rebuild(void Function(ProductDtoBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ProductDtoBuilder toBuilder() => new ProductDtoBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ProductDto &&
        id == other.id &&
        title == other.title &&
        price == other.price &&
        description == other.description &&
        createdAt == other.createdAt &&
        pictures == other.pictures &&
        seller == other.seller;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc($jc($jc(0, id.hashCode), title.hashCode),
                        price.hashCode),
                    description.hashCode),
                createdAt.hashCode),
            pictures.hashCode),
        seller.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ProductDto')
          ..add('id', id)
          ..add('title', title)
          ..add('price', price)
          ..add('description', description)
          ..add('createdAt', createdAt)
          ..add('pictures', pictures)
          ..add('seller', seller))
        .toString();
  }
}

class ProductDtoBuilder implements Builder<ProductDto, ProductDtoBuilder> {
  _$ProductDto _$v;

  int _id;
  int get id => _$this._id;
  set id(int id) => _$this._id = id;

  String _title;
  String get title => _$this._title;
  set title(String title) => _$this._title = title;

  double _price;
  double get price => _$this._price;
  set price(double price) => _$this._price = price;

  String _description;
  String get description => _$this._description;
  set description(String description) => _$this._description = description;

  DateTime _createdAt;
  DateTime get createdAt => _$this._createdAt;
  set createdAt(DateTime createdAt) => _$this._createdAt = createdAt;

  ListBuilder<PictureDto> _pictures;
  ListBuilder<PictureDto> get pictures =>
      _$this._pictures ??= new ListBuilder<PictureDto>();
  set pictures(ListBuilder<PictureDto> pictures) => _$this._pictures = pictures;

  SellerDtoBuilder _seller;
  SellerDtoBuilder get seller => _$this._seller ??= new SellerDtoBuilder();
  set seller(SellerDtoBuilder seller) => _$this._seller = seller;

  ProductDtoBuilder();

  ProductDtoBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _title = _$v.title;
      _price = _$v.price;
      _description = _$v.description;
      _createdAt = _$v.createdAt;
      _pictures = _$v.pictures?.toBuilder();
      _seller = _$v.seller?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ProductDto other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ProductDto;
  }

  @override
  void update(void Function(ProductDtoBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ProductDto build() {
    _$ProductDto _$result;
    try {
      _$result = _$v ??
          new _$ProductDto._(
              id: id,
              title: title,
              price: price,
              description: description,
              createdAt: createdAt,
              pictures: _pictures?.build(),
              seller: _seller?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'pictures';
        _pictures?.build();
        _$failedField = 'seller';
        _seller?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'ProductDto', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
