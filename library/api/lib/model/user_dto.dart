        import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'user_dto.g.dart';

abstract class UserDto implements Built<UserDto, UserDtoBuilder> {

    
        @nullable
    @BuiltValueField(wireName: r'id')
    int get id;
    
        @nullable
    @BuiltValueField(wireName: r'name')
    String get name;
    
        @nullable
    @BuiltValueField(wireName: r'email')
    String get email;
    
        @nullable
    @BuiltValueField(wireName: r'phoneNumber')
    String get phoneNumber;
    
        @nullable
    @BuiltValueField(wireName: r'avatar')
    String get avatar;

    // Boilerplate code needed to wire-up generated code
    UserDto._();

    factory UserDto([updates(UserDtoBuilder b)]) = _$UserDto;
    static Serializer<UserDto> get serializer => _$userDtoSerializer;

}

