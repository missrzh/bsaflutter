        import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'product_list_dto.g.dart';

abstract class ProductListDto implements Built<ProductListDto, ProductListDtoBuilder> {

    
        @nullable
    @BuiltValueField(wireName: r'id')
    int get id;
    
        @nullable
    @BuiltValueField(wireName: r'title')
    String get title;
    
        @nullable
    @BuiltValueField(wireName: r'price')
    double get price;
    
        @nullable
    @BuiltValueField(wireName: r'description')
    String get description;
    
        @nullable
    @BuiltValueField(wireName: r'picture')
    String get picture;

    // Boilerplate code needed to wire-up generated code
    ProductListDto._();

    factory ProductListDto([updates(ProductListDtoBuilder b)]) = _$ProductListDto;
    static Serializer<ProductListDto> get serializer => _$productListDtoSerializer;

}

