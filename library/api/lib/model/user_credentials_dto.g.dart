// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_credentials_dto.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<UserCredentialsDto> _$userCredentialsDtoSerializer =
    new _$UserCredentialsDtoSerializer();

class _$UserCredentialsDtoSerializer
    implements StructuredSerializer<UserCredentialsDto> {
  @override
  final Iterable<Type> types = const [UserCredentialsDto, _$UserCredentialsDto];
  @override
  final String wireName = 'UserCredentialsDto';

  @override
  Iterable<Object> serialize(Serializers serializers, UserCredentialsDto object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.email != null) {
      result
        ..add('email')
        ..add(serializers.serialize(object.email,
            specifiedType: const FullType(String)));
    }
    if (object.password != null) {
      result
        ..add('password')
        ..add(serializers.serialize(object.password,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  UserCredentialsDto deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new UserCredentialsDtoBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'email':
          result.email = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'password':
          result.password = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$UserCredentialsDto extends UserCredentialsDto {
  @override
  final String email;
  @override
  final String password;

  factory _$UserCredentialsDto(
          [void Function(UserCredentialsDtoBuilder) updates]) =>
      (new UserCredentialsDtoBuilder()..update(updates)).build();

  _$UserCredentialsDto._({this.email, this.password}) : super._();

  @override
  UserCredentialsDto rebuild(
          void Function(UserCredentialsDtoBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  UserCredentialsDtoBuilder toBuilder() =>
      new UserCredentialsDtoBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is UserCredentialsDto &&
        email == other.email &&
        password == other.password;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, email.hashCode), password.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('UserCredentialsDto')
          ..add('email', email)
          ..add('password', password))
        .toString();
  }
}

class UserCredentialsDtoBuilder
    implements Builder<UserCredentialsDto, UserCredentialsDtoBuilder> {
  _$UserCredentialsDto _$v;

  String _email;
  String get email => _$this._email;
  set email(String email) => _$this._email = email;

  String _password;
  String get password => _$this._password;
  set password(String password) => _$this._password = password;

  UserCredentialsDtoBuilder();

  UserCredentialsDtoBuilder get _$this {
    if (_$v != null) {
      _email = _$v.email;
      _password = _$v.password;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(UserCredentialsDto other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$UserCredentialsDto;
  }

  @override
  void update(void Function(UserCredentialsDtoBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$UserCredentialsDto build() {
    final _$result =
        _$v ?? new _$UserCredentialsDto._(email: email, password: password);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
