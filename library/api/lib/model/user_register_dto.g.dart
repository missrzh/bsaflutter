// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_register_dto.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<UserRegisterDto> _$userRegisterDtoSerializer =
    new _$UserRegisterDtoSerializer();

class _$UserRegisterDtoSerializer
    implements StructuredSerializer<UserRegisterDto> {
  @override
  final Iterable<Type> types = const [UserRegisterDto, _$UserRegisterDto];
  @override
  final String wireName = 'UserRegisterDto';

  @override
  Iterable<Object> serialize(Serializers serializers, UserRegisterDto object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.email != null) {
      result
        ..add('email')
        ..add(serializers.serialize(object.email,
            specifiedType: const FullType(String)));
    }
    if (object.name != null) {
      result
        ..add('name')
        ..add(serializers.serialize(object.name,
            specifiedType: const FullType(String)));
    }
    if (object.phoneNumber != null) {
      result
        ..add('phoneNumber')
        ..add(serializers.serialize(object.phoneNumber,
            specifiedType: const FullType(String)));
    }
    if (object.password != null) {
      result
        ..add('password')
        ..add(serializers.serialize(object.password,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  UserRegisterDto deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new UserRegisterDtoBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'email':
          result.email = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'phoneNumber':
          result.phoneNumber = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'password':
          result.password = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$UserRegisterDto extends UserRegisterDto {
  @override
  final String email;
  @override
  final String name;
  @override
  final String phoneNumber;
  @override
  final String password;

  factory _$UserRegisterDto([void Function(UserRegisterDtoBuilder) updates]) =>
      (new UserRegisterDtoBuilder()..update(updates)).build();

  _$UserRegisterDto._({this.email, this.name, this.phoneNumber, this.password})
      : super._();

  @override
  UserRegisterDto rebuild(void Function(UserRegisterDtoBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  UserRegisterDtoBuilder toBuilder() =>
      new UserRegisterDtoBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is UserRegisterDto &&
        email == other.email &&
        name == other.name &&
        phoneNumber == other.phoneNumber &&
        password == other.password;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, email.hashCode), name.hashCode), phoneNumber.hashCode),
        password.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('UserRegisterDto')
          ..add('email', email)
          ..add('name', name)
          ..add('phoneNumber', phoneNumber)
          ..add('password', password))
        .toString();
  }
}

class UserRegisterDtoBuilder
    implements Builder<UserRegisterDto, UserRegisterDtoBuilder> {
  _$UserRegisterDto _$v;

  String _email;
  String get email => _$this._email;
  set email(String email) => _$this._email = email;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  String _phoneNumber;
  String get phoneNumber => _$this._phoneNumber;
  set phoneNumber(String phoneNumber) => _$this._phoneNumber = phoneNumber;

  String _password;
  String get password => _$this._password;
  set password(String password) => _$this._password = password;

  UserRegisterDtoBuilder();

  UserRegisterDtoBuilder get _$this {
    if (_$v != null) {
      _email = _$v.email;
      _name = _$v.name;
      _phoneNumber = _$v.phoneNumber;
      _password = _$v.password;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(UserRegisterDto other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$UserRegisterDto;
  }

  @override
  void update(void Function(UserRegisterDtoBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$UserRegisterDto build() {
    final _$result = _$v ??
        new _$UserRegisterDto._(
            email: email,
            name: name,
            phoneNumber: phoneNumber,
            password: password);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
