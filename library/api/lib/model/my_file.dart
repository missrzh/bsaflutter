        import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'my_file.g.dart';

abstract class MyFile implements Built<MyFile, MyFileBuilder> {

    
        @nullable
    @BuiltValueField(wireName: r'fileName')
    String get fileName;
    
        @nullable
    @BuiltValueField(wireName: r'content')
    String get content;

    // Boilerplate code needed to wire-up generated code
    MyFile._();

    factory MyFile([updates(MyFileBuilder b)]) = _$MyFile;
    static Serializer<MyFile> get serializer => _$myFileSerializer;

}

