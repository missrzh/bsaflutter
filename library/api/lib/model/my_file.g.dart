// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'my_file.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<MyFile> _$myFileSerializer = new _$MyFileSerializer();

class _$MyFileSerializer implements StructuredSerializer<MyFile> {
  @override
  final Iterable<Type> types = const [MyFile, _$MyFile];
  @override
  final String wireName = 'MyFile';

  @override
  Iterable<Object> serialize(Serializers serializers, MyFile object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.fileName != null) {
      result
        ..add('fileName')
        ..add(serializers.serialize(object.fileName,
            specifiedType: const FullType(String)));
    }
    if (object.content != null) {
      result
        ..add('content')
        ..add(serializers.serialize(object.content,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  MyFile deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new MyFileBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'fileName':
          result.fileName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'content':
          result.content = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$MyFile extends MyFile {
  @override
  final String fileName;
  @override
  final String content;

  factory _$MyFile([void Function(MyFileBuilder) updates]) =>
      (new MyFileBuilder()..update(updates)).build();

  _$MyFile._({this.fileName, this.content}) : super._();

  @override
  MyFile rebuild(void Function(MyFileBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  MyFileBuilder toBuilder() => new MyFileBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is MyFile &&
        fileName == other.fileName &&
        content == other.content;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, fileName.hashCode), content.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('MyFile')
          ..add('fileName', fileName)
          ..add('content', content))
        .toString();
  }
}

class MyFileBuilder implements Builder<MyFile, MyFileBuilder> {
  _$MyFile _$v;

  String _fileName;
  String get fileName => _$this._fileName;
  set fileName(String fileName) => _$this._fileName = fileName;

  String _content;
  String get content => _$this._content;
  set content(String content) => _$this._content = content;

  MyFileBuilder();

  MyFileBuilder get _$this {
    if (_$v != null) {
      _fileName = _$v.fileName;
      _content = _$v.content;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(MyFile other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$MyFile;
  }

  @override
  void update(void Function(MyFileBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$MyFile build() {
    final _$result =
        _$v ?? new _$MyFile._(fileName: fileName, content: content);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
