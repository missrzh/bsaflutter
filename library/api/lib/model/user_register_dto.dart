        import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'user_register_dto.g.dart';

abstract class UserRegisterDto implements Built<UserRegisterDto, UserRegisterDtoBuilder> {

    
        @nullable
    @BuiltValueField(wireName: r'email')
    String get email;
    
        @nullable
    @BuiltValueField(wireName: r'name')
    String get name;
    
        @nullable
    @BuiltValueField(wireName: r'phoneNumber')
    String get phoneNumber;
    
        @nullable
    @BuiltValueField(wireName: r'password')
    String get password;

    // Boilerplate code needed to wire-up generated code
    UserRegisterDto._();

    factory UserRegisterDto([updates(UserRegisterDtoBuilder b)]) = _$UserRegisterDto;
    static Serializer<UserRegisterDto> get serializer => _$userRegisterDtoSerializer;

}

