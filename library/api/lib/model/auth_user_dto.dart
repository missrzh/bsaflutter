        import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'auth_user_dto.g.dart';

abstract class AuthUserDto implements Built<AuthUserDto, AuthUserDtoBuilder> {

    
        @nullable
    @BuiltValueField(wireName: r'id')
    int get id;
    
        @nullable
    @BuiltValueField(wireName: r'token')
    String get token;
    
        @nullable
    @BuiltValueField(wireName: r'email')
    String get email;
    
        @nullable
    @BuiltValueField(wireName: r'name')
    String get name;
    
        @nullable
    @BuiltValueField(wireName: r'role')
    String get role;

    // Boilerplate code needed to wire-up generated code
    AuthUserDto._();

    factory AuthUserDto([updates(AuthUserDtoBuilder b)]) = _$AuthUserDto;
    static Serializer<AuthUserDto> get serializer => _$authUserDtoSerializer;

}

