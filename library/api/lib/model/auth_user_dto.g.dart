// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'auth_user_dto.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<AuthUserDto> _$authUserDtoSerializer = new _$AuthUserDtoSerializer();

class _$AuthUserDtoSerializer implements StructuredSerializer<AuthUserDto> {
  @override
  final Iterable<Type> types = const [AuthUserDto, _$AuthUserDto];
  @override
  final String wireName = 'AuthUserDto';

  @override
  Iterable<Object> serialize(Serializers serializers, AuthUserDto object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.id != null) {
      result
        ..add('id')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(int)));
    }
    if (object.token != null) {
      result
        ..add('token')
        ..add(serializers.serialize(object.token,
            specifiedType: const FullType(String)));
    }
    if (object.email != null) {
      result
        ..add('email')
        ..add(serializers.serialize(object.email,
            specifiedType: const FullType(String)));
    }
    if (object.name != null) {
      result
        ..add('name')
        ..add(serializers.serialize(object.name,
            specifiedType: const FullType(String)));
    }
    if (object.role != null) {
      result
        ..add('role')
        ..add(serializers.serialize(object.role,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  AuthUserDto deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new AuthUserDtoBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'token':
          result.token = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'email':
          result.email = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'role':
          result.role = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$AuthUserDto extends AuthUserDto {
  @override
  final int id;
  @override
  final String token;
  @override
  final String email;
  @override
  final String name;
  @override
  final String role;

  factory _$AuthUserDto([void Function(AuthUserDtoBuilder) updates]) =>
      (new AuthUserDtoBuilder()..update(updates)).build();

  _$AuthUserDto._({this.id, this.token, this.email, this.name, this.role})
      : super._();

  @override
  AuthUserDto rebuild(void Function(AuthUserDtoBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  AuthUserDtoBuilder toBuilder() => new AuthUserDtoBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is AuthUserDto &&
        id == other.id &&
        token == other.token &&
        email == other.email &&
        name == other.name &&
        role == other.role;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc($jc(0, id.hashCode), token.hashCode), email.hashCode),
            name.hashCode),
        role.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('AuthUserDto')
          ..add('id', id)
          ..add('token', token)
          ..add('email', email)
          ..add('name', name)
          ..add('role', role))
        .toString();
  }
}

class AuthUserDtoBuilder implements Builder<AuthUserDto, AuthUserDtoBuilder> {
  _$AuthUserDto _$v;

  int _id;
  int get id => _$this._id;
  set id(int id) => _$this._id = id;

  String _token;
  String get token => _$this._token;
  set token(String token) => _$this._token = token;

  String _email;
  String get email => _$this._email;
  set email(String email) => _$this._email = email;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  String _role;
  String get role => _$this._role;
  set role(String role) => _$this._role = role;

  AuthUserDtoBuilder();

  AuthUserDtoBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _token = _$v.token;
      _email = _$v.email;
      _name = _$v.name;
      _role = _$v.role;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(AuthUserDto other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$AuthUserDto;
  }

  @override
  void update(void Function(AuthUserDtoBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$AuthUserDto build() {
    final _$result = _$v ??
        new _$AuthUserDto._(
            id: id, token: token, email: email, name: name, role: role);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
