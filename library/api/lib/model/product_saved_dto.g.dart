// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'product_saved_dto.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<ProductSavedDto> _$productSavedDtoSerializer =
    new _$ProductSavedDtoSerializer();

class _$ProductSavedDtoSerializer
    implements StructuredSerializer<ProductSavedDto> {
  @override
  final Iterable<Type> types = const [ProductSavedDto, _$ProductSavedDto];
  @override
  final String wireName = 'ProductSavedDto';

  @override
  Iterable<Object> serialize(Serializers serializers, ProductSavedDto object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.id != null) {
      result
        ..add('id')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(int)));
    }
    if (object.title != null) {
      result
        ..add('title')
        ..add(serializers.serialize(object.title,
            specifiedType: const FullType(String)));
    }
    if (object.price != null) {
      result
        ..add('price')
        ..add(serializers.serialize(object.price,
            specifiedType: const FullType(double)));
    }
    if (object.description != null) {
      result
        ..add('description')
        ..add(serializers.serialize(object.description,
            specifiedType: const FullType(String)));
    }
    if (object.createdAt != null) {
      result
        ..add('createdAt')
        ..add(serializers.serialize(object.createdAt,
            specifiedType: const FullType(DateTime)));
    }
    if (object.pictures != null) {
      result
        ..add('pictures')
        ..add(serializers.serialize(object.pictures,
            specifiedType:
                const FullType(BuiltList, const [const FullType(PictureDto)])));
    }
    return result;
  }

  @override
  ProductSavedDto deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ProductSavedDtoBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'title':
          result.title = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'price':
          result.price = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
        case 'description':
          result.description = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'createdAt':
          result.createdAt = serializers.deserialize(value,
              specifiedType: const FullType(DateTime)) as DateTime;
          break;
        case 'pictures':
          result.pictures.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(PictureDto)]))
              as BuiltList<dynamic>);
          break;
      }
    }

    return result.build();
  }
}

class _$ProductSavedDto extends ProductSavedDto {
  @override
  final int id;
  @override
  final String title;
  @override
  final double price;
  @override
  final String description;
  @override
  final DateTime createdAt;
  @override
  final BuiltList<PictureDto> pictures;

  factory _$ProductSavedDto([void Function(ProductSavedDtoBuilder) updates]) =>
      (new ProductSavedDtoBuilder()..update(updates)).build();

  _$ProductSavedDto._(
      {this.id,
      this.title,
      this.price,
      this.description,
      this.createdAt,
      this.pictures})
      : super._();

  @override
  ProductSavedDto rebuild(void Function(ProductSavedDtoBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ProductSavedDtoBuilder toBuilder() =>
      new ProductSavedDtoBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ProductSavedDto &&
        id == other.id &&
        title == other.title &&
        price == other.price &&
        description == other.description &&
        createdAt == other.createdAt &&
        pictures == other.pictures;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc($jc($jc($jc(0, id.hashCode), title.hashCode), price.hashCode),
                description.hashCode),
            createdAt.hashCode),
        pictures.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ProductSavedDto')
          ..add('id', id)
          ..add('title', title)
          ..add('price', price)
          ..add('description', description)
          ..add('createdAt', createdAt)
          ..add('pictures', pictures))
        .toString();
  }
}

class ProductSavedDtoBuilder
    implements Builder<ProductSavedDto, ProductSavedDtoBuilder> {
  _$ProductSavedDto _$v;

  int _id;
  int get id => _$this._id;
  set id(int id) => _$this._id = id;

  String _title;
  String get title => _$this._title;
  set title(String title) => _$this._title = title;

  double _price;
  double get price => _$this._price;
  set price(double price) => _$this._price = price;

  String _description;
  String get description => _$this._description;
  set description(String description) => _$this._description = description;

  DateTime _createdAt;
  DateTime get createdAt => _$this._createdAt;
  set createdAt(DateTime createdAt) => _$this._createdAt = createdAt;

  ListBuilder<PictureDto> _pictures;
  ListBuilder<PictureDto> get pictures =>
      _$this._pictures ??= new ListBuilder<PictureDto>();
  set pictures(ListBuilder<PictureDto> pictures) => _$this._pictures = pictures;

  ProductSavedDtoBuilder();

  ProductSavedDtoBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _title = _$v.title;
      _price = _$v.price;
      _description = _$v.description;
      _createdAt = _$v.createdAt;
      _pictures = _$v.pictures?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ProductSavedDto other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ProductSavedDto;
  }

  @override
  void update(void Function(ProductSavedDtoBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ProductSavedDto build() {
    _$ProductSavedDto _$result;
    try {
      _$result = _$v ??
          new _$ProductSavedDto._(
              id: id,
              title: title,
              price: price,
              description: description,
              createdAt: createdAt,
              pictures: _pictures?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'pictures';
        _pictures?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'ProductSavedDto', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
