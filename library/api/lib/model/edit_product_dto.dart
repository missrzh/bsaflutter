        import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'edit_product_dto.g.dart';

abstract class EditProductDto implements Built<EditProductDto, EditProductDtoBuilder> {

    
        @nullable
    @BuiltValueField(wireName: r'title')
    String get title;
    
        @nullable
    @BuiltValueField(wireName: r'price')
    double get price;
    
        @nullable
    @BuiltValueField(wireName: r'description')
    String get description;

    // Boilerplate code needed to wire-up generated code
    EditProductDto._();

    factory EditProductDto([updates(EditProductDtoBuilder b)]) = _$EditProductDto;
    static Serializer<EditProductDto> get serializer => _$editProductDtoSerializer;

}

