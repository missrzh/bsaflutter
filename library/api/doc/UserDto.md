# openapi.model.UserDto

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] [default to null]
**name** | **String** |  | [optional] [default to null]
**email** | **String** |  | [optional] [default to null]
**phoneNumber** | **String** |  | [optional] [default to null]
**avatar** | **String** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


