# openapi.api.UsersApi

## Load the API package
```dart
import 'package:openapi/api.dart';
```

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiUsersAvatarAsStringPost**](UsersApi.md#apiUsersAvatarAsStringPost) | **post** /api/Users/avatar/asString | 
[**apiUsersAvatarPost**](UsersApi.md#apiUsersAvatarPost) | **post** /api/Users/avatar | 
[**apiUsersDetailsGet**](UsersApi.md#apiUsersDetailsGet) | **get** /api/Users/details | 


# **apiUsersAvatarAsStringPost**
> String apiUsersAvatarAsStringPost(myFile)



### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: Bearer
//defaultApiClient.getAuthentication<HttpBasicAuth>('Bearer').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('Bearer').password = 'YOUR_PASSWORD';

var api_instance = new UsersApi();
var myFile = new MyFile(); // MyFile | 

try { 
    var result = api_instance.apiUsersAvatarAsStringPost(myFile);
    print(result);
} catch (e) {
    print("Exception when calling UsersApi->apiUsersAvatarAsStringPost: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **myFile** | [**MyFile**](MyFile.md)|  | [optional] 

### Return type

**String**

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/_*+json
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **apiUsersAvatarPost**
> String apiUsersAvatarPost(file)



### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: Bearer
//defaultApiClient.getAuthentication<HttpBasicAuth>('Bearer').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('Bearer').password = 'YOUR_PASSWORD';

var api_instance = new UsersApi();
var file = BINARY_DATA_HERE; // Uint8List | 

try { 
    var result = api_instance.apiUsersAvatarPost(file);
    print(result);
} catch (e) {
    print("Exception when calling UsersApi->apiUsersAvatarPost: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **file** | **Uint8List**|  | [optional] [default to null]

### Return type

**String**

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **apiUsersDetailsGet**
> UserDto apiUsersDetailsGet()



### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: Bearer
//defaultApiClient.getAuthentication<HttpBasicAuth>('Bearer').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('Bearer').password = 'YOUR_PASSWORD';

var api_instance = new UsersApi();

try { 
    var result = api_instance.apiUsersDetailsGet();
    print(result);
} catch (e) {
    print("Exception when calling UsersApi->apiUsersDetailsGet: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**UserDto**](UserDto.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

