# openapi.model.ProductListDto

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] [default to null]
**title** | **String** |  | [optional] [default to null]
**price** | **double** |  | [optional] [default to null]
**description** | **String** |  | [optional] [default to null]
**picture** | **String** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


