# openapi.api.AuthApi

## Load the API package
```dart
import 'package:openapi/api.dart';
```

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiAuthLoginPost**](AuthApi.md#apiAuthLoginPost) | **post** /api/Auth/login | 
[**apiAuthRegisterPost**](AuthApi.md#apiAuthRegisterPost) | **post** /api/Auth/register | 


# **apiAuthLoginPost**
> AuthUserDto apiAuthLoginPost(userCredentialsDto)



### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: Bearer
//defaultApiClient.getAuthentication<HttpBasicAuth>('Bearer').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('Bearer').password = 'YOUR_PASSWORD';

var api_instance = new AuthApi();
var userCredentialsDto = new UserCredentialsDto(); // UserCredentialsDto | 

try { 
    var result = api_instance.apiAuthLoginPost(userCredentialsDto);
    print(result);
} catch (e) {
    print("Exception when calling AuthApi->apiAuthLoginPost: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userCredentialsDto** | [**UserCredentialsDto**](UserCredentialsDto.md)|  | [optional] 

### Return type

[**AuthUserDto**](AuthUserDto.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/_*+json
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **apiAuthRegisterPost**
> UserDto apiAuthRegisterPost(userRegisterDto)



### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: Bearer
//defaultApiClient.getAuthentication<HttpBasicAuth>('Bearer').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('Bearer').password = 'YOUR_PASSWORD';

var api_instance = new AuthApi();
var userRegisterDto = new UserRegisterDto(); // UserRegisterDto | 

try { 
    var result = api_instance.apiAuthRegisterPost(userRegisterDto);
    print(result);
} catch (e) {
    print("Exception when calling AuthApi->apiAuthRegisterPost: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userRegisterDto** | [**UserRegisterDto**](UserRegisterDto.md)|  | [optional] 

### Return type

[**UserDto**](UserDto.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/_*+json
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

