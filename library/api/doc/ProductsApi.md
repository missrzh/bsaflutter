# openapi.api.ProductsApi

## Load the API package
```dart
import 'package:openapi/api.dart';
```

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiProductsGet**](ProductsApi.md#apiProductsGet) | **get** /api/Products | 
[**apiProductsIdDelete**](ProductsApi.md#apiProductsIdDelete) | **delete** /api/Products/{id} | 
[**apiProductsIdGet**](ProductsApi.md#apiProductsIdGet) | **get** /api/Products/{id} | 
[**apiProductsIdPut**](ProductsApi.md#apiProductsIdPut) | **put** /api/Products/{id} | 
[**apiProductsPost**](ProductsApi.md#apiProductsPost) | **post** /api/Products | 


# **apiProductsGet**
> List<ProductListDto> apiProductsGet(page, perPage, filter)



### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: Bearer
//defaultApiClient.getAuthentication<HttpBasicAuth>('Bearer').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('Bearer').password = 'YOUR_PASSWORD';

var api_instance = new ProductsApi();
var page = 56; // int | 
var perPage = 56; // int | 
var filter = filter_example; // String | 

try { 
    var result = api_instance.apiProductsGet(page, perPage, filter);
    print(result);
} catch (e) {
    print("Exception when calling ProductsApi->apiProductsGet: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **int**|  | [optional] [default to null]
 **perPage** | **int**|  | [optional] [default to null]
 **filter** | **String**|  | [optional] [default to null]

### Return type

[**List<ProductListDto>**](ProductListDto.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **apiProductsIdDelete**
> apiProductsIdDelete(id)



### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: Bearer
//defaultApiClient.getAuthentication<HttpBasicAuth>('Bearer').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('Bearer').password = 'YOUR_PASSWORD';

var api_instance = new ProductsApi();
var id = 56; // int | 

try { 
    api_instance.apiProductsIdDelete(id);
} catch (e) {
    print("Exception when calling ProductsApi->apiProductsIdDelete: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  | [default to null]

### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **apiProductsIdGet**
> ProductDto apiProductsIdGet(id)



### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: Bearer
//defaultApiClient.getAuthentication<HttpBasicAuth>('Bearer').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('Bearer').password = 'YOUR_PASSWORD';

var api_instance = new ProductsApi();
var id = 56; // int | 

try { 
    var result = api_instance.apiProductsIdGet(id);
    print(result);
} catch (e) {
    print("Exception when calling ProductsApi->apiProductsIdGet: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  | [default to null]

### Return type

[**ProductDto**](ProductDto.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **apiProductsIdPut**
> ProductSavedDto apiProductsIdPut(id, editProductDto)



### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: Bearer
//defaultApiClient.getAuthentication<HttpBasicAuth>('Bearer').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('Bearer').password = 'YOUR_PASSWORD';

var api_instance = new ProductsApi();
var id = 56; // int | 
var editProductDto = new EditProductDto(); // EditProductDto | 

try { 
    var result = api_instance.apiProductsIdPut(id, editProductDto);
    print(result);
} catch (e) {
    print("Exception when calling ProductsApi->apiProductsIdPut: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  | [default to null]
 **editProductDto** | [**EditProductDto**](EditProductDto.md)|  | [optional] 

### Return type

[**ProductSavedDto**](ProductSavedDto.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/_*+json
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **apiProductsPost**
> ProductSavedDto apiProductsPost(editProductDto)



### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: Bearer
//defaultApiClient.getAuthentication<HttpBasicAuth>('Bearer').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('Bearer').password = 'YOUR_PASSWORD';

var api_instance = new ProductsApi();
var editProductDto = new EditProductDto(); // EditProductDto | 

try { 
    var result = api_instance.apiProductsPost(editProductDto);
    print(result);
} catch (e) {
    print("Exception when calling ProductsApi->apiProductsPost: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **editProductDto** | [**EditProductDto**](EditProductDto.md)|  | [optional] 

### Return type

[**ProductSavedDto**](ProductSavedDto.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/_*+json
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

