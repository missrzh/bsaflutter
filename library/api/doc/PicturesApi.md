# openapi.api.PicturesApi

## Load the API package
```dart
import 'package:openapi/api.dart';
```

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiProductsProductIdPicturesAsStringPost**](PicturesApi.md#apiProductsProductIdPicturesAsStringPost) | **post** /api/Products/{productId}/Pictures/asString | 
[**apiProductsProductIdPicturesIdDelete**](PicturesApi.md#apiProductsProductIdPicturesIdDelete) | **delete** /api/Products/{productId}/Pictures/{id} | 
[**apiProductsProductIdPicturesPost**](PicturesApi.md#apiProductsProductIdPicturesPost) | **post** /api/Products/{productId}/Pictures | 


# **apiProductsProductIdPicturesAsStringPost**
> PictureDto apiProductsProductIdPicturesAsStringPost(productId, myFile)



### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: Bearer
//defaultApiClient.getAuthentication<HttpBasicAuth>('Bearer').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('Bearer').password = 'YOUR_PASSWORD';

var api_instance = new PicturesApi();
var productId = 56; // int | 
var myFile = new MyFile(); // MyFile | 

try { 
    var result = api_instance.apiProductsProductIdPicturesAsStringPost(productId, myFile);
    print(result);
} catch (e) {
    print("Exception when calling PicturesApi->apiProductsProductIdPicturesAsStringPost: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productId** | **int**|  | [default to null]
 **myFile** | [**MyFile**](MyFile.md)|  | [optional] 

### Return type

[**PictureDto**](PictureDto.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/_*+json
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **apiProductsProductIdPicturesIdDelete**
> apiProductsProductIdPicturesIdDelete(id, productId)



### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: Bearer
//defaultApiClient.getAuthentication<HttpBasicAuth>('Bearer').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('Bearer').password = 'YOUR_PASSWORD';

var api_instance = new PicturesApi();
var id = 56; // int | 
var productId = 56; // int | 

try { 
    api_instance.apiProductsProductIdPicturesIdDelete(id, productId);
} catch (e) {
    print("Exception when calling PicturesApi->apiProductsProductIdPicturesIdDelete: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  | [default to null]
 **productId** | **int**|  | [default to null]

### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **apiProductsProductIdPicturesPost**
> PictureDto apiProductsProductIdPicturesPost(productId, file)



### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: Bearer
//defaultApiClient.getAuthentication<HttpBasicAuth>('Bearer').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('Bearer').password = 'YOUR_PASSWORD';

var api_instance = new PicturesApi();
var productId = 56; // int | 
var file = BINARY_DATA_HERE; // Uint8List | 

try { 
    var result = api_instance.apiProductsProductIdPicturesPost(productId, file);
    print(result);
} catch (e) {
    print("Exception when calling PicturesApi->apiProductsProductIdPicturesPost: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productId** | **int**|  | [default to null]
 **file** | **Uint8List**|  | [optional] [default to null]

### Return type

[**PictureDto**](PictureDto.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

