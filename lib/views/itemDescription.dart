import 'package:flutter/material.dart';

class ItemDescription extends StatelessWidget {
  const ItemDescription({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Stack(
          children: [
            ListView(
              children: [
                Container(
                  height: 400,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      fit: BoxFit.cover,
                      image: NetworkImage(
                          'https://upload.wikimedia.org/wikipedia/en/6/60/No_Picture.jpg'),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 10, left: 10),
                  child: Text(
                    "Title",
                    style: const TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 20.0,
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 10, left: 10),
                  child: Text(
                    "LOooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooong description",
                    style: const TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 14.0,
                    ),
                  ),
                ),
              ],
            ),
            Positioned(
              bottom: 0,
              child: Container(
                height: 100,
                child: Column(
                  children: [
                    Row(
                      children: [
                        ClipRRect(
                          borderRadius: BorderRadius.circular(100.0),
                          child: Image.network(
                            'https://icon-library.com/images/no-picture-available-icon/no-picture-available-icon-20.jpg',
                            height: 50.0,
                            width: 50.0,
                          ),
                        ),
                        Column(
                          children: [
                            Text("Seller Name"),
                            Text('+ 38 098 567 98 73')
                          ],
                        ),
                      ],
                    ),
                    Row(
                      children: [Text('Call seller'), Text('Add to bag')],
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
