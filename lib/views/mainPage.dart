import 'package:customserverapi/model/product_list_dto.dart';
import 'package:flutter/material.dart';
import 'package:flutter_application_1/bloc/products_bloc.dart';
import 'package:flutter_application_1/views/parts.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final ProductsBloc bloc = ProductsBloc();
  Widget _buildProducts(List<ProductListDto> products) {
    return Container(
      color: Colors.black87,
      child: Center(
        child: ListView.builder(
            itemCount: products.length,
            itemBuilder: (context, index) {
              return CustomListItem(
                subtitle: products[index].description,
                price: products[index].price,
                image: products[index].picture,
                title: products[index].title,
              );
            }),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    bloc.add(ProductStartSearch());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: Text(widget.title),
      ),
      body: BlocProvider(
        create: (context) => bloc,
        child: BlocBuilder<ProductsBloc, ProductsState>(
          builder: (context, state) {
            print(state);
            if (state.runtimeType == ProductsLoaded) {
              return _buildProducts((state as ProductsLoaded).products);
            } else {
              return Center(
                child: SpinKitCubeGrid(
                  itemBuilder: (BuildContext context, int index) {
                    return DecoratedBox(
                      decoration: BoxDecoration(
                        color: Colors.black,
                      ),
                    );
                  },
                ),
              );
            }
          },
        ),
      ),
    );
  }
}
