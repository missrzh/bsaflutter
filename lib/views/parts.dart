import 'package:flutter/material.dart';

class CustomListItem extends StatelessWidget {
  const CustomListItem({
    Key key,
    this.image,
    this.title,
    this.subtitle,
    this.price,
  }) : super(key: key);

  final String image;
  final String title;
  final String subtitle;
  final double price;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 20, left: 20),
      child: InkWell(
        onTap: () async {
          final result =
              await Navigator.of(context).pushNamed('/ItemDescription');
        },
        child: Container(
          decoration: new BoxDecoration(
            color: Colors.grey,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20),
              bottomLeft: Radius.circular(20),
            ),
          ),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                flex: 2,
                child: Container(
                  height: 100,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20),
                        bottomLeft: Radius.circular(20)),
                    image: DecorationImage(
                      fit: BoxFit.cover,
                      image: NetworkImage(image != null
                          ? image
                          : 'https://upload.wikimedia.org/wikipedia/en/6/60/No_Picture.jpg'),
                    ),
                  ),
                ),
              ),
              Expanded(
                flex: 3,
                child: _ProductDescription(
                  title: title,
                  user: subtitle,
                  viewCount: price,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class _ProductDescription extends StatelessWidget {
  const _ProductDescription({
    Key key,
    this.title,
    this.user,
    this.viewCount,
  }) : super(key: key);

  final String title;
  final String user;
  final double viewCount;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 5.0, left: 5.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            title,
            style: const TextStyle(
              fontWeight: FontWeight.w500,
              fontSize: 14.0,
            ),
            overflow: TextOverflow.ellipsis,
            maxLines: 1,
          ),
          const Padding(padding: EdgeInsets.symmetric(vertical: 2.0)),
          Text(
            'Price: $viewCount \$',
            style: const TextStyle(fontSize: 10.0),
          ),
          Text(
            user,
            style: const TextStyle(fontSize: 10.0),
            overflow: TextOverflow.ellipsis,
            maxLines: 3,
          ),
          const Padding(padding: EdgeInsets.symmetric(vertical: 1.0)),
        ],
      ),
    );
  }
}
