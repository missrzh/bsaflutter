part of 'products_bloc.dart';

@immutable
abstract class ProductsEvent {}

class ProductStartSearch extends ProductsEvent {}
