part of 'products_bloc.dart';

@immutable
abstract class ProductsState {}

class ProductsInitial extends ProductsState {}

class ProductsLoaded extends ProductsState {
  final List<ProductListDto> products;
  ProductsLoaded(this.products);
}

class ProductsLoading extends ProductsState {}

class ProductsError extends ProductsState {}
