import 'package:bloc/bloc.dart';
import 'package:customserverapi/api.dart';
import 'package:customserverapi/model/product_dto.dart';
import 'package:customserverapi/model/product_list_dto.dart';
import 'package:meta/meta.dart';

part 'products_event.dart';
part 'products_state.dart';

class ProductsBloc extends Bloc<ProductsEvent, ProductsState> {
  final api = Openapi();
  ProductsBloc() : super(ProductsInitial()) {
    on<ProductsEvent>((event, emit) async {
      emit(ProductsLoading());
      var r_products = await api
          .getProductsApi()
          .apiProductsGet(page: 1, perPage: 20, headers: {
        "Authorization":
            'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6Im1pc2hhbjE2MkBnbWFpbC5jb20iLCJuYW1laWQiOiI0MiIsIm5iZiI6MTY0MzQ4OTgwNywiZXhwIjoxNjQ3MDg5ODA3LCJpYXQiOjE2NDM0ODk4MDd9.7qMEmPzU43SP-JVrWBjhNKK-1U8_Z53RztZmmUkG4Vo'
      });

      emit(ProductsLoaded(r_products.data));
    });
  }
}
