import 'package:flutter/material.dart';
import 'package:flutter_application_1/views/itemDescription.dart';
import 'package:flutter_application_1/views/mainPage.dart';
import 'package:customserverapi/api.dart';

void main() {
  runApp(MyApp());
}

final api = Openapi();

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(onGenerateRoute: (settings) {
      WidgetBuilder builder;
      switch (settings.name) {
        case '/':
          builder = (context) => MyHomePage(title: 'Shop');
          break;
        case '/ItemDescription':
          builder = (context) => ItemDescription();
          break;
        default:
      }
      return MaterialPageRoute(builder: builder);
    });
  }
}
